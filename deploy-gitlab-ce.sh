#!/bin/bash

#install Gitlab CE

yum -y update
yum install -y curl policycoreutils-python epel-release
yum -y install nginx
yum install -y postfix
systemctl enable postfix
systemctl start postfix
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-Ce/script.rpm.sh | bash



sudo EXTERNAL_URL="https://ci.cookingwithazure.com" yum install -y gitlab-ce

cat <<EOF >> /etc/gitlab/gitlab.rb
letsencrypt['enable'] = true
letsencrypt['contact_emails'] = ['alessandro.vozza@microsoft.com'] # Optional
nginx['redirect_http_to_https'] = yes

gitlab_rails['omniauth_enabled'] = true
gitlab_rails['omniauth_allow_single_sign_on'] = ['azure_oauth2']
gitlab_rails['omniauth_block_auto_created_users'] = false
gitlab_rails['sync_profile_from_provider'] = ['azure_oauth2']
gitlab_rails['sync_profile_attributes'] = ['name', 'email']
gitlab_rails['omniauth_external_providers'] = ['azure_oauth2']
gitlab_rails['omniauth_providers'] = [
  {
    "name" => "azure_oauth2",
    "args" => {
      "client_id" => "CLIENT_ID",
      "client_secret" => "CLIENT_SECRET",
      "tenant_id" => "TENANT_ID",
    }
  }
]

EOF
gitlab-ctl reconfigure
gitlab-ctl restart
